package main;

public class Deal implements Comparable<Deal> {
    private Game game;
    private String country;
    private double factor, difference, weight;

    public Deal(Game game, String country, double factor, double difference) {
        this.game = game;
        this.country = country;
        this.factor = factor;
        this.difference = difference;
        weight = factor * 100*100 / (Math.pow( game.getRating(), 2.0 ));
    }

    public Game getGame() {
        return game;
    }

    public String getCountry() {
        return country;
    }

    public double getFactor() {
        return factor;
    }

    public double getPrice() {
        return game.getPrices().get( country );
    }

    public double getDifference() {
        return difference;
    }

    @Override
    public int compareTo(Deal o) {
        if( this.getFactor() == o.getFactor() )
            return 0;
        if( this.getFactor() < o.getFactor() )
            return -1;
        return 1;
    }

    public double getWeight() {
        return weight;
    }
}
