package main;

import java.util.ArrayList;
import java.util.List;

public class Regions {
    private List<List<String>> regions;

    public Regions() {
        regions = new ArrayList<>();

        List<String> europe = new ArrayList<>();
        europe.add("Austria");
        europe.add("Belgium");
        europe.add("Bulgaria");
        europe.add("Croatia");
        europe.add("Cyprus");
        europe.add("Czech Republic");
        europe.add("Denmark");
        europe.add("Estonia");
        europe.add("Finland");
        europe.add("France");
        europe.add("Germany");
        europe.add("Greece");
        europe.add("Hungary");
        europe.add("Ireland");
        europe.add("Italy");
        europe.add("Latvia");
        europe.add("Lithuania");
        europe.add("Luxembourg");
        europe.add("Malta");
        europe.add("Netherlands");
        europe.add("New Zealand");
        europe.add("Norway");
        europe.add("Poland");
        europe.add("Portugal");
        europe.add("Romania");
        europe.add("Russia");
        europe.add("Slovakia");
        europe.add("Slovenia");
        europe.add("South Africa");
        europe.add("Spain");
        europe.add("Sweden");
        europe.add("Switzerland");
        europe.add("United Kingdom");

        regions.add( europe );
    }

    public List<String> getRegion(String country) {
        for( List<String> region : regions ) {
            for( String regionCountry : region ) {
                if( regionCountry.equals( country ) )
                    return region;
            }
        }
        return null;
    }
}
