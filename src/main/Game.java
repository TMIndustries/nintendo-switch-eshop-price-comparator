package main;

import java.util.HashMap;
import java.util.List;

public class Game {
    private String name;
    private HashMap<String, Double> prices;
    private int rating;

    public Game(String name, HashMap<String, Double> prices, int rating) {
        this.name = name;
        this.prices = prices;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public HashMap<String, Double> getPrices() {
        return prices;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
