package main;

import java.util.List;

public class DealBroker {
    private List<String> whitelist;

    public DealBroker(List<String> whitelist) {
        this.whitelist = whitelist;
    }

    public Deal findBestDeal(Game game, String forCountry) {
        Deal deal = null;
        double initialPrice = -1;
        boolean gameAvailableInCountry = false;
        if (game.getPrices().containsKey( forCountry )) {
            deal = new Deal(game, forCountry, 1, 0);
            initialPrice = game.getPrices().get(forCountry);
            gameAvailableInCountry = true;
        }

        if( initialPrice == 0 )
            return deal;

        for( String country : game.getPrices().keySet() ) {
            if( country.equals( forCountry ) ) continue;
            if( whitelist != null && !whitelist.contains( country ) ) continue;

            double price = game.getPrices().get( country );

            if( price == 0 ) {
                deal = new Deal(game, country, Double.MAX_VALUE, initialPrice - price);
            } else {
                if( !gameAvailableInCountry ) {
                    if( deal == null || price < deal.getPrice() )
                        deal = new Deal( game, country, 1, 0 );
                } else {
                    double factor = price / initialPrice;
                    if( price < deal.getPrice() )
                        deal = new Deal( game, country, factor, initialPrice - price );
                }
            }
        }

        if( !gameAvailableInCountry ) {
            if( deal != null ) {
                //System.out.println( "[NotAvailable] " + game.getName() );
            } else {
                //System.out.println( "[NotBuyable] " + game.getName() );
            }
        }
        return deal;
    }
}
