package main;

import dealcomparators.AbsoluteComparator;
import dealcomparators.RatingComparator;
import dealcomparators.RelativeComparator;
import dealcomparators.WeightedComparator;
import eshopprices.GamesOnSaleScraper;
import eshopprices.Source;
import metacritic.MetaScraper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    public static final String AGENT = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2";
    private String homeCountry = "Germany";
    private String currency = "€";
    private int minimumRating = 75;

    public static void main(String args[]) {
        new Main();
    }

    public Main() {
        try {

            Source source = new Source();
            Source table = extractTable(source);
            List<Source> tableRows = extractTableRows(table);

            Source headerRow = tableRows.get(0);
            tableRows.remove(0);
            List<String> countryNames = extractCountryNames( headerRow );
            countryNames.forEach(System.out::println);

            List<Game> games = extractGames( tableRows, countryNames );
            List<Game> gamesOnSale = mapNameToGame( gamesOnSale(), games );
            System.out.println( "Number of compared games: " + games.size() );
            System.out.println( "Number of games on sale: " + gamesOnSale.size() );

            while( true ) {
                Scanner scanner = new Scanner(System.in);
                System.out.println( "Region Filter?" );
                String inputRegion = scanner.nextLine();
                System.out.println( "Number of results?" );
                String inputResults = scanner.nextLine();
                System.out.println();

                boolean regionFilter = inputRegion.equals("true") || inputRegion.equals("1");
                int numberOfDeals = Integer.parseInt( inputResults );

                List<Deal> deals = getBestDealsByCountry(games, homeCountry, regionFilter);
                List<Deal> dealsOnSale = getBestDealsByCountry(gamesOnSale, homeCountry, regionFilter);

                System.out.println("::: Games on Sale (international) :::");
                printDeals(dealsOnSale, numberOfDeals, "Prozent und Wertung", new WeightedComparator(minimumRating));
                printDeals(dealsOnSale, numberOfDeals, "Prozent", new RelativeComparator(minimumRating));
                printDeals(dealsOnSale, numberOfDeals, "Wertung", new RatingComparator(minimumRating));
                printDeals(dealsOnSale, numberOfDeals, "absolute Einsparung", new AbsoluteComparator(minimumRating));

                System.out.println("::: Best games bought international :::");
                printDeals(deals, numberOfDeals, "Prozent und Wertung", new WeightedComparator(minimumRating));
                printDeals(deals, numberOfDeals, "Prozent", new RelativeComparator(minimumRating));
                printDeals(deals, numberOfDeals, "Wertung", new RatingComparator(minimumRating));
                printDeals(deals, numberOfDeals, "absolute Einsparung", new AbsoluteComparator(minimumRating));

                System.out.println();
                System.out.println("Continue?");
                String inputContinue = scanner.nextLine();
                switch( inputContinue ) {
                    case "true":
                    case "1":
                    case "continue":
                        continue;
                        default:
                            break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Game> mapNameToGame(List<String> gamesOnSale, List<Game> games) {
        List<Game> list = new ArrayList<>();
        for( String gameOnSale : gamesOnSale ) {
            Game found = null;
            for( Game game : games ) {
                if( game.getName().equals( gameOnSale ) ) {
                    found = game;
                    break;
                }
            }

            if( found != null )
            list.add( found );
            else
                System.err.println( "Game on sale is not in list: " + gameOnSale );
        }
        return list;
    }

    private void printDeals(List<Deal> deals, int numberOfDeals, String name, Comparator<Deal> sorter) {
        deals.sort(sorter);

        System.out.println( ">>> Top " + numberOfDeals + " ("+name+")" );
        for( int i = 0; i < numberOfDeals; i++ ) {
            Deal deal = deals.get( i );
            System.out.println("[" + formatRating(deal.getGame().getRating()) + "|" + formatPrice(deal.getPrice()) + "|" + getDiscount(deal.getFactor()) + "] " + deal.getGame().getName() + " in " + deal.getCountry() + " (" + formatWeight(deal.getWeight()) + ")"  );
        }
        System.out.println();
    }

    private String formatWeight(double weight) {
        NumberFormat formatter = new DecimalFormat("0.00");
        return formatter.format(weight);
    }

    private String getDiscount(double factor) {
        return String.valueOf( (int)(-(1.0 - factor)*100) ) + "%";
    }

    private String formatPrice(double price) {
        NumberFormat formatter = new DecimalFormat("0.00");
        return formatter.format( price ) + currency;
    }

    private String formatRating(int rating) {
        NumberFormat f = new DecimalFormat("###");
        return f.format(rating);
    }

    private List<Deal> getBestDealsByCountry(List<Game> games, String byCountry, boolean regionFilter) {
        List<Deal> deals = new ArrayList<>(games.size());

        List<String> whitelist = null;
        if( regionFilter )
        whitelist = new Regions().getRegion(homeCountry);

        DealBroker broker = new DealBroker(whitelist);

        for( Game game : games ) {
            Deal bestDeal = broker.findBestDeal( game, byCountry );
            if( bestDeal != null )
            deals.add( bestDeal );
        }

        //deals.sort(null);

        return deals;
    }

    private List<Game> extractGames(List<Source> tableRows, List<String> countryNames) {
        List<Game> games = new ArrayList<>();
        Pattern gameNamePattern = Pattern.compile( "(<th.*?><a.*?>)(.*?)(</a>)" );
        Pattern gameSeriesPattern = Pattern.compile( "<span.*?>(.*?)</span>(.*)" );
        Pattern priceCellPattern = Pattern.compile( "(<td.*?>)(.*?)(</td>)" );
        Pattern pricePattern = Pattern.compile( "(?:" + currency + ")?" + "((?:\\d+,\\d{2})|(?:N/A))" );

        for( Source row : tableRows ) {
            String gameName = row.apply( gameNamePattern, 2 ).get(0).toString();

            Matcher m = gameSeriesPattern.matcher(gameName);
            if( m.find() )
                gameName = m.group(1) + " " + m.group(2);

            List<Source> sourcePrices = row.apply( priceCellPattern, 2 );
            HashMap<String, Double> prices = new HashMap<>();

            for (int i = 0; i < sourcePrices.size(); i++) {
                Source sourcePrice = sourcePrices.get(i);
                String rawPrice = sourcePrice.apply(pricePattern, 1).get(0).toString();
                if ("N/A".equals(rawPrice)) {
                    continue;
                } else {
                    rawPrice = rawPrice.replace( ',', '.' );
                    Double price = Double.parseDouble(rawPrice);
                    prices.put( countryNames.get(i), price );
                }
            }

            games.add( new Game( gameName, prices, 0 ) );
        }

        refreshRatings(games);

        return games;
    }

    private void refreshRatings(List<Game> games) {
        MetaScraper scraper = new MetaScraper();

        AtomicInteger counter = new AtomicInteger(0);

        games.parallelStream().forEach(game -> {
            int rating = 0;
            try {
                for( int i = 0; ; i++ ) {
                    try {
                        rating = scraper.scrapeRating(game.getName());
                        break;
                        //System.out.println( "[" + formatRating(rating) + "]" + game.getName() );
                    } catch (IOException e) {
                        if (e instanceof FileNotFoundException) {
                            System.err.println("["+ counter.getAndIncrement() +"] No rating for: " + game.getName() + " | " + e.toString());
                            break;
                        }
                        Thread.sleep(2500);
                        if( i >= 10 ) {
                            e.printStackTrace();
                            break;
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            game.setRating(rating);
        });

        System.err.println( counter.get() + " games have no rating!" );
    }

    private List<String> extractCountryNames(Source headerRow) {
        Pattern pattern = Pattern.compile( "(<th.+?title=\"(.+?)\">)(.*?)(</th>)" );
        List<Source> countries = headerRow.apply( pattern, 2 );
        List<String> countryNames = countries.stream().map(Source::toString).collect(Collectors.toList());
        return countryNames;
    }

    private List<Source> extractTableRows(Source table) {
        Pattern pattern = Pattern.compile( "(<tr.*?>)(.*?)(</tr>)" );
        return table.apply( pattern, 2 );
    }

    private Source extractTable(Source source) {
        Pattern pattern = Pattern.compile( "(<table.*?>)(.*?)(</table>)" );
        return source.apply( pattern, 2 ).get(0);
    }

    private List<String> gamesOnSale() throws IOException {
        GamesOnSaleScraper scraper = new GamesOnSaleScraper();
        return scraper.refreshList();
    }
}
