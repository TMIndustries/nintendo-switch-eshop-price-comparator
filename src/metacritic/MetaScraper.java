package metacritic;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static main.Main.AGENT;

public class MetaScraper {
    public int scrapeRating(String gameName) throws IOException {
        gameName = manualMapping(gameName);

        try {
            return scrape( gameName, true );
        } catch( FileNotFoundException e) {
            return scrape( gameName, false );
        }
    }

    private int scrape(String gameName, boolean removeMisc) throws IOException {
        URL metacriticURL = buildURL( gameName, removeMisc );

        Pattern pattern = Pattern.compile("<span itemprop=\"ratingValue\">(.*?)</span>");
        String rawRating = scrape( metacriticURL, pattern, 1 );

        if( rawRating == null )
            return 0;

        return Integer.parseInt( rawRating );
    }

    private String scrape(URL metacriticURL, Pattern pattern, int group) throws IOException {
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            URLConnection connection = metacriticURL.openConnection();
            connection.setRequestProperty("User-Agent", AGENT);
            connection.connect();
            is = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                Matcher m = pattern.matcher( line );
                if( m.find() ) {
                    return m.group(group);
                }
            }
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }

        return null;
    }

    private String normalizeURLName(String string, boolean removeMisc) {
        String normalized = string;
        normalized = normalized.toLowerCase();

        if( removeMisc )
            normalized = removeMisc(normalized);

        normalized = Normalizer.normalize(normalized, Normalizer.Form.NFD);
        normalized = normalized.replaceAll("[^a-zA-Z0-9!\\-+ ]", "");
        normalized = normalized.trim();
        normalized = normalized.replaceAll("[ ]{2,}", " ");
        normalized = normalized.replace( ' ', '-' );
        return normalized;
    }

    private String removeMisc(String misc) {
        misc = misc.replace( "Digital Version".toLowerCase(), "" );
        misc = misc.replace( "Digital Edition".toLowerCase(), "" );
        misc = misc.replace( "for Nintendo Switch".toLowerCase(), "" );
        return misc;
    }

    private URL buildURL(String gameName, boolean removeMisc) throws MalformedURLException {
        String normalized = normalizeURLName( gameName, removeMisc );
        String urlPath = "https://www.metacritic.com/game/switch/" + normalized;
        return new URL( urlPath );
    }

    private String manualMapping(String gameName) {
        switch( gameName ) {
            case "The Binding of Isaac: Afterbirth+":
                return "The Binding of Isaac: Afterbirth +";
            case "Minecraft":
                return "Minecraft: Switch Edition";
            case "Cave Story+":
                return "Cave Story +";
            default:
                return gameName;
        }
    }
}
