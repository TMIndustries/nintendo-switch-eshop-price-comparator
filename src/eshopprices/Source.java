package eshopprices;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static main.Main.AGENT;

public class Source {
    private String sourceURL = "https://eshop-prices.com/prices?currency=EUR";
    private String source;

    public Source() throws IOException {
            URL url = new URL(sourceURL);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", AGENT);
            connection.connect();
            InputStream is = connection.getInputStream();
            try(BufferedReader reader = new BufferedReader( new InputStreamReader(is) ) ) {
                source = "";
                String line;
                while ((line = reader.readLine()) != null) {
                    source += line;
                }
            }
    }

    private Source(String source) {
        this.source = source;
    }

    public List<Source> apply(Pattern pattern, int group) {
        Matcher m = pattern.matcher( source );
        List<Source> results = new ArrayList<>();
        while( m.find() ) {
            Source result = new Source( m.group(group) );
            results.add( result );
        }
        return results;
    }

    @Override
    public String toString() {
        return source;
    }
}
