package eshopprices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static main.Main.AGENT;

public class GamesOnSaleScraper {
    private String url = "https://eshop-prices.com/games/on-sale";
    private String phpPage = "?page=";
    private List<String> allGamesOnSale;

    public GamesOnSaleScraper() {

    }

    public List<String> refreshList() throws IOException {
        List<String> allGamesOnSale = new ArrayList<>();

        int page = 1;
        while(true) {
            BufferedReader reader = connect(page++);
            List<String> scrapedGames = scrape( reader );
            if( scrapedGames.size() == 0 )
                break;
            allGamesOnSale.addAll(scrapedGames);
        }

        this.allGamesOnSale = allGamesOnSale;
        return this.allGamesOnSale;
    }

    private BufferedReader connect(int page) throws IOException {
        URL url = new URL(this.url + phpPage + page);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent", AGENT);
        connection.connect();
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader( new InputStreamReader(is) );
        return reader;
    }

    private List<String> scrape(BufferedReader reader) {
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile( "<h5 class=\"games-list-item-title\">(.*?)</h5>" );
        Pattern gameSeries = Pattern.compile( "<span class=\"game-serie\">(.*?)</span>(.*)" );
        reader.lines().map( pattern::matcher ).forEach( m -> {
            while( m.find() ) {
                String gameName = m.group(1);

                Matcher seriesM = gameSeries.matcher(gameName);
                if( seriesM.find() ) {
                    gameName = seriesM.group(1) + " " + seriesM.group(2);
                }

                list.add( gameName );
            }
        } );
        return list;
    }
}
