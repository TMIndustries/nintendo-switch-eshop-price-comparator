package dealcomparators;

import main.Deal;

import java.util.Comparator;

public class WeightedComparator extends MinimumRatingComparator  {
    public WeightedComparator(int minimumRating) {
        super(minimumRating);
    }

    @Override
    public int compareStage2(Deal o1, Deal o2) {
        double o1Weight = o1.getWeight();
        double o2Weight = o2.getWeight();

        if( o1Weight == o2Weight )
            return 0;
        if( o1Weight < o2Weight )
            return -1;
        return 1;
    }
}
