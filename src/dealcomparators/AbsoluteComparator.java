package dealcomparators;

import main.Deal;

import java.util.Comparator;

public class AbsoluteComparator extends MinimumRatingComparator {
    public AbsoluteComparator(int minimumRating) {
        super(minimumRating);
    }

    @Override
    public int compareStage2(Deal o1, Deal o2) {
        if( o1.getDifference() == o2.getDifference() )
            return 0;
        if( o1.getDifference() < o2.getDifference() )
            return 1;
        return -1;
    }
}
