package dealcomparators;

import main.Deal;

import java.util.Comparator;

public class RelativeComparator extends MinimumRatingComparator  {
    public RelativeComparator(int minimumRating) {
        super(minimumRating);
    }

    @Override
    public int compareStage2(Deal o1, Deal o2) {
        if( o1.getFactor() == o2.getFactor() )
            return 0;
        if( o1.getFactor() < o2.getFactor() )
            return -1;
        return 1;
    }
}
