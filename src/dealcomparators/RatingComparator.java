package dealcomparators;

import main.Deal;

import java.util.Comparator;

public class RatingComparator extends MinimumRatingComparator  {
    public RatingComparator(int minimumRating) {
        super(minimumRating);
    }

    @Override
    public int compareStage2(Deal o1, Deal o2) {
        if( o1.getGame().getRating() == o2.getGame().getRating() )
            return 0;
        if( o1.getGame().getRating() > o2.getGame().getRating() )
            return -1;
        return 1;
    }
}
