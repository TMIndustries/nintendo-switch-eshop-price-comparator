package dealcomparators;

import main.Deal;

import java.util.Comparator;

public abstract class MinimumRatingComparator implements Comparator<Deal> {
    private int minimumRating;

    public MinimumRatingComparator(int minimumRating) {
        this.minimumRating = minimumRating;
    }

    @Override
    public int compare(Deal o1, Deal o2) {
        if( o1.getGame().getRating() >= minimumRating && o2.getGame().getRating() < minimumRating )
            return -1;
        if( o2.getGame().getRating() >= minimumRating && o1.getGame().getRating() < minimumRating )
            return 1;

        return compareStage2(o1, o2);
    }

    protected abstract int compareStage2(Deal o1, Deal o2);
}
